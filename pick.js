var options = {};

function balance(){
	var total = 0;
	for (var item in options){
		total += options[item];
	}
}

function render(){
	var list = $("#options");
	list.empty();
	for (var item in options){
		var e = $('<li>', {id: item, text: item + ' (' + options[item] + ')', value: options[item]});
		e.click(function(){
			var name = $(this).attr('id');
			$("#name").val(name);
			$("#value").val(options[name]);
			$("#add input[value=Remove]").show();
		});
		list.append(e);
	}
}

function del(){
	var name = $('#name').val();
	if (!name){ return; }
	$('#name').val('');
	$('#value').val('');
	delete options[name];
	render();
	$('#add input[value=Remove]').hide();
}

function add(){
	$("#warn").hide();
	$("#add input[value=Remove]").hide();
	var name = $("#name").val();
	var value = parseFloat($("#value").val());
	if (!name){ return }
	else if (value < 1 || isNaN(value)){
		$("#warn").text('Invalid percentage. Are you sure?');
		$("#warn").show();
		return;
	}
	$("#name").val('');
	$("#value").val('');
	options[name] = value;
	balance();
	render();
	$("#count").val(parseInt($("#count").val())+1);
}

function spin(){
	var spectrum = {};
	var index = 0;
	for (var item in options){
		spectrum[item] = 0;
	}
	var picked = Math.random() * 100;
}

$(function(){
});
//http://anthonyterrien.com/demo/knob/
//
//https://github.com/softwaretailoring/wheelnav/releases
//http://wtabs.softwaretailoring.net/
//
//https://blog.bramp.net/wheel/
//http://codepen.io/AndreCortellini/pen/vERwmL?editors=0010
//http://www.jqueryscript.net/blog/8-Newest-Free-jQuery-Plugins-For-This-Week-9-2015.html
